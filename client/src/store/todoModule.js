
export default {
  state: () => ({
    todos: []
  }),
  getters: {
    getTodos (state) {
      return state.todos
    }
  },
  mutations: {
    addTodo (state, todo) {
      state.todos.push(todo)
    },
    deleteTodo (state, index) {
      state.todos.splice(index, 1)
    },
    changeStatus (state, index) {
      state.todos[index].done = !state.todos[index].done
    },
    changeTodo (state, payload) {
      state.todos[payload.index].todo = payload.newTodo
    }
  },
  actions: {
  }
}
